Hey there!

We hope you're as thrilled as we are about the upcoming Literary Fest "Lexis" on 13-15 Oct 2023. Get ready for some serious fun, because Slashdot has something special planned just for you!

We'll be setting up a fabulous club stall (on 14-15th Oct) where you can unleash your competitive spirit and play some super entertaining, **small but fun games**. And guess what? You'll have a chance to win some truly exciting prizes! So gather your friends and join us at our stall to make unforgettable memories.

But wait, that's not all! We have two incredible workshops lined up exclusively for you.

1. **Personal Blogging Website Workshop:**\
    On the 14th, don't miss our hands-on workshop on creating your personal blogging website using *HUGO*! Learn the art of web development, and by the end of the session, you'll have your very own blog ready to launch.

2. **Pixel Art Workshop:**\
    And on the 15th, dive into the world of pixels at our *Pixel Art Workshop*! Discover the magic of creating stunning visuals one pixel at a time. Unleash your creativity and bring your imagination to life.

But that's not it. We're also hosting **Slashdot Chan** (an exciting doodling competition) where we'll be deciding the Mascot for our club! Show off your doodling skills and create a unique character along with their captivating backstory. The winning entry will become the face of Slashdot, representing us in all our future endeavours.

So mark your calendars and join us at the fest from 14th to 15th Oct 2023. It's going to be an event filled with laughter, learning, and endless opportunities!

If you have any questions or need more information about any of these events, feel free to reach out to us anytime.

Stay tuned for more updates on our social media platforms too!

Let's make this fest one for the books!
